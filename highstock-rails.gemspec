$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "highstock/rails/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "highstock-rails"
  s.version     = Highstock::Rails::VERSION
  s.authors     = ["Takeshi Hui"]
  s.email       = ["takeshihui@gmail.com"]
  s.homepage    = ""
  s.summary     = "Highstock 7.0.1"
  s.description = "Highstock 7.0.1"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", "~> 5.1.6", ">= 5.1.6.1"

  s.add_development_dependency "sqlite3"
end
