require "highstock/rails/version"

module Highstock
  module Rails
    class Error < StandardError; end
    class Engine < ::Rails::Engine; end
  end
end
